from matplotlib import pyplot
from matplotlib.image import imread

# define location of dataset
DATASET_FOLDER = 'datasets/dogs-vs-cats/train/'

def plot_dogs():
	# plot first few images
	for i in range(9):
		# define subplot
		pyplot.subplot(330 + 1 + i)
		# define filename
		filename = DATASET_FOLDER + 'dog.' + str(i) + '.jpg'
		# load image pixels
		image = imread(filename)
		# plot raw pixel data
		pyplot.imshow(image)
	# show the figure
	pyplot.show()


def plot_cats():
	# plot first few images
	for i in range(9):
		# define subplot
		pyplot.subplot(330 + 1 + i)
		# define filename
		filename = DATASET_FOLDER + 'cat.' + str(i) + '.jpg'
		# load image pixels
		image = imread(filename)
		# plot raw pixel data
		pyplot.imshow(image)
	# show the figure
	pyplot.show()


def plot_dogs_and_cats():
	# plot first few dogs images
	for i in range(4):
		print(i)
		pyplot.subplot(330 + 1 + i)
		filename = DATASET_FOLDER + 'dog.' + str(i) + '.jpg'
		image = imread(filename)
		pyplot.imshow(image)
	print()
	# plot first few cats images
	for i in range(4, 9):
		print(i)
		pyplot.subplot(330 + 1 + i)
		filename = DATASET_FOLDER + 'cat.' + str(i) + '.jpg'
		image = imread(filename)
		pyplot.imshow(image)

	# show the figure
	pyplot.show()


if __name__ == "__main__":
	# plot_cats()
	# plot_dogs()
	plot_dogs_and_cats()
